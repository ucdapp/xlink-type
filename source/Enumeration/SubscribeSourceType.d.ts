// @formatter:off
declare const enum SubscribeSourceType {
  Discover = 1,
  Share = 2,
  Code = 3,
  Home = 4,
  Manual = 5,
  WeChat = 6
}
// @formatter:on

export default SubscribeSourceType;
