import SubscribeSourceType from '../Enumeration/SubscribeSourceType';

export default interface Device {
  access_key: number;
  active_code: string;
  active_date: string;
  authority?: string;
  authorize_code: string;
  firmware_mod: string;
  firmware_version: number;
  id: number;
  is_active: boolean;
  is_online: boolean;
  last_login: string;
  mac: string;
  mcu_mod: string;
  mcu_version: number;
  name: string;
  product_id: string;
  role?: number;
  sn?: string;
  soft_init_date?: string;
  source?: SubscribeSourceType;
}
